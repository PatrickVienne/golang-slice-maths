# Results of Benchmark Tests comparing floating point and integer slice operations

```shell
$ go test -bench=. -run=^# -gcflags="-d=ssa/check_bce" -benchmem
# weightedvotes [weightedvotes.test]
./weighted_votes.go:15:30: Found IsInBounds
./weighted_votes.go:27:31: Found IsInBounds
./weighted_votes.go:27:44: Found IsInBounds
./weighted_votes_test.go:24:29: Found IsInBounds
goos: windows
goarch: amd64
pkg: weightedvotes
cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
Benchmark_getWeightedVote-8                     261553483                4.541 ns/op           0 B/op          0 allocs/op
Benchmark_getWeightedVoteInts-8                     4933            257408 ns/op             325 B/op          0 allocs/op
Benchmark_getWeightedVoteIntsParallel-8               10         155629980 ns/op        10560652 B/op     200003 allocs/op
Benchmark_getWeightedVoteBce-8                  55574079                19.02 ns/op            0 B/op          0 allocs/op
Benchmark_getWeightedVoteBlas64-8               15039636                73.68 ns/op            0 B/op          0 allocs/op
Benchmark_getWeightedVoteGonumMat-8              1620242               660.7 ns/op            96 B/op          2 allocs/op
Benchmark_getWeightedVoteBlas64Vec-8            17709084                76.69 ns/op            0 B/op          0 allocs/op
Benchmark_getWeightedVoteGonumVecMat-8           2789190               473.0 ns/op             0 B/op          0 allocs/op
Benchmark_getWeightedVoteLarge-8                   15226             91858 ns/op             105 B/op          0 allocs/op
Benchmark_getWeightedVoteIntsLarge-8                5715            239946 ns/op             280 B/op          0 allocs/op
Benchmark_getWeightedVoteIntsParallelLarge-8           7         158865071 ns/op        10646518 B/op     200044 allocs/op
Benchmark_getWeightedVoteBceLarge-8                 1627            735627 ns/op             986 B/op          0 allocs/op
Benchmark_getWeightedVoteBlas64Large-8              1899            632595 ns/op             845 B/op          0 allocs/op
Benchmark_getWeightedVoteGonumMatLarge-8            2306            554743 ns/op             792 B/op          2 allocs/op
Benchmark_getWeightedVoteBlas64VecLarge-8           1425           1108050 ns/op            1126 B/op          0 allocs/op
Benchmark_getWeightedVoteGonumVecMatLarge-8         2011            643290 ns/op             798 B/op          0 allocs/op
PASS
ok      weightedvotes   36.706s
```

For additional escape analysis use:
```shell
$ go test -bench=. -run=^# -gcflags="-d=ssa/check_bce -m"
```
https://medium.com/a-journey-with-go/go-introduction-to-the-escape-analysis-f7610174e890

To get profiles:
```shell
$ go test -memprofile=mem.prof -cpuprofile=cpu.prof -trace=trace.out
```