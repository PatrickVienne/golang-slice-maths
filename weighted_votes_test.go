package weightedvotes

import (
	"testing"

	"gonum.org/v1/gonum/blas/blas64"
	"gonum.org/v1/gonum/mat"
)

func Test_getWeightedVote(t *testing.T) {
	type args struct {
		votes   []float64
		weights []float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "Test01", args: args{votes: []float64{0, 100}, weights: []float64{99, 1}}, want: 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getWeightedVote(tt.args.votes, tt.args.weights); got != tt.want {
				t.Errorf("getWeightedVote() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getWeightedVoteBce(t *testing.T) {
	type args struct {
		votes   []float64
		weights []float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "Test01", args: args{votes: []float64{0, 100}, weights: []float64{99, 1}}, want: 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getWeightedVoteBce(tt.args.votes, tt.args.weights); got != tt.want {
				t.Errorf("getWeightedVoteBce() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getWeightedVoteBlas64(t *testing.T) {
	type args struct {
		votes   []float64
		weights []float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "Test01", args: args{votes: []float64{0, 100}, weights: []float64{99, 1}}, want: 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getWeightedVoteBlas64(tt.args.votes, tt.args.weights); got != tt.want {
				t.Errorf("getWeightedVoteBlas64() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getWeightedVoteGonumMat(t *testing.T) {
	type args struct {
		votes   []float64
		weights []float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "Test01", args: args{votes: []float64{0, 100}, weights: []float64{99, 1}}, want: 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getWeightedVoteGonumMat(tt.args.votes, tt.args.weights); got != tt.want {
				t.Errorf("getWeightedVoteGonumMat() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Benchmark_getWeightedVote(b *testing.B) {
	votes, weights := getSmallArray()
	for n := 0; n < b.N; n++ {
		getWeightedVote(votes, weights)
	}
}

func Benchmark_getWeightedVoteInts(b *testing.B) {
	votes, weights := getLargeArrayInt()
	for n := 0; n < b.N; n++ {
		getWeightedVoteInts(votes, weights)
	}
}

func Benchmark_getWeightedVoteIntsParallel(b *testing.B) {
	votes, weights := getLargeArrayInt()
	for n := 0; n < b.N; n++ {
		getWeightedVoteIntsParallel(votes, weights)
	}
}

func Benchmark_getWeightedVoteBce(b *testing.B) {
	votes, weights := getSmallArray()
	for n := 0; n < b.N; n++ {
		getWeightedVoteBce(votes, weights)
	}
}

func Benchmark_getWeightedVoteBlas64(b *testing.B) {
	votes, weights := getSmallArray()
	for n := 0; n < b.N; n++ {
		getWeightedVoteBlas64(votes, weights)
	}
}

func Benchmark_getWeightedVoteGonumMat(b *testing.B) {
	votes, weights := getSmallArray()
	for n := 0; n < b.N; n++ {
		getWeightedVoteGonumMat(votes, weights)
	}
}

func Benchmark_getWeightedVoteBlas64Vec(b *testing.B) {
	votes, weights := getSmallArray()
	vec1 := blas64.Vector{
		N:    len(votes),
		Data: votes,
		Inc:  1,
	}
	vec2 := blas64.Vector{
		N:    len(weights),
		Data: weights,
		Inc:  1,
	}
	for n := 0; n < b.N; n++ {
		getWeightedVoteBlas64Vec(vec1, vec2)
	}
}

func Benchmark_getWeightedVoteGonumVecMat(b *testing.B) {
	votes, weights := getSmallArray()
	vec1 := mat.NewVecDense(len(votes), votes)
	vec2 := mat.NewVecDense(len(weights), weights)
	for n := 0; n < b.N; n++ {
		getWeightedVoteGonumVecMat(vec1, vec2)
	}
}

func getSmallArrayInt() ([]int64, []int64) {
	return []int64{0, 100}, []int64{0, 100}
}

func getLargeArrayInt() ([]int64, []int64) {
	const N = 100000
	a := make([]int64, N)
	b := make([]int64, N)
	for idx := range a {
		a[idx] = int64(idx) * 10000
		b[idx] = int64(idx) * 10000
	}
	return a, b
}

func getSmallArray() ([]float64, []float64) {
	return []float64{0, 100}, []float64{0, 100}
}

func getLargeArray() ([]float64, []float64) {
	const N = 100000
	a := make([]float64, N)
	b := make([]float64, N)
	for idx := range a {
		a[idx] = float64(idx) * 10000
		b[idx] = float64(idx) * 10000
	}
	return a, b
}

func Benchmark_getWeightedVoteLarge(b *testing.B) {
	votes, weights := getLargeArray()
	for n := 0; n < b.N; n++ {
		getWeightedVote(votes, weights)
	}
}

func Benchmark_getWeightedVoteIntsLarge(b *testing.B) {
	votes, weights := getLargeArrayInt()
	for n := 0; n < b.N; n++ {
		getWeightedVoteInts(votes, weights)
	}
}

func Benchmark_getWeightedVoteIntsParallelLarge(b *testing.B) {
	votes, weights := getLargeArrayInt()
	for n := 0; n < b.N; n++ {
		getWeightedVoteIntsParallel(votes, weights)
	}
}

func Benchmark_getWeightedVoteBceLarge(b *testing.B) {
	votes, weights := getLargeArray()
	for n := 0; n < b.N; n++ {
		getWeightedVoteBce(votes, weights)
	}
}

func Benchmark_getWeightedVoteBlas64Large(b *testing.B) {
	votes, weights := getLargeArray()
	for n := 0; n < b.N; n++ {
		getWeightedVoteBlas64(votes, weights)
	}
}

func Benchmark_getWeightedVoteGonumMatLarge(b *testing.B) {
	votes, weights := getLargeArray()
	for n := 0; n < b.N; n++ {
		getWeightedVoteGonumMat(votes, weights)
	}
}

func Benchmark_getWeightedVoteBlas64VecLarge(b *testing.B) {
	votes, weights := getLargeArray()
	vec1 := blas64.Vector{
		N:    len(votes),
		Data: votes,
		Inc:  1,
	}
	vec2 := blas64.Vector{
		N:    len(weights),
		Data: weights,
		Inc:  1,
	}
	for n := 0; n < b.N; n++ {
		getWeightedVoteBlas64Vec(vec1, vec2)
	}
}

func Benchmark_getWeightedVoteGonumVecMatLarge(b *testing.B) {
	votes, weights := getLargeArray()
	vec1 := mat.NewVecDense(len(votes), votes)
	vec2 := mat.NewVecDense(len(weights), weights)
	for n := 0; n < b.N; n++ {
		getWeightedVoteGonumVecMat(vec1, vec2)
	}
}
