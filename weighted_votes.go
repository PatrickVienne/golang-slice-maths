package weightedvotes

import (
	"fmt"
	"sync"
	"sync/atomic"

	"gonum.org/v1/gonum/blas/blas64"
	"gonum.org/v1/gonum/mat"
)

func getWeightedVoteInts(votes, weights []int64) int64 {
	var res, allWeights int64
	for idx := range votes {
		res += votes[idx] * weights[idx]
		allWeights += weights[idx]
	}
	return res / allWeights
}

func getWeightedVoteIntsParallel(votes, weights []int64) int64 {
	var wg sync.WaitGroup
	wg.Add(len(votes))
	var res, allWeights int64
	for idx := range votes {
		go func(idx int) {
			atomic.AddInt64(&res, votes[idx]*weights[idx])
			atomic.AddInt64(&allWeights, weights[idx])
			wg.Done()
		}(idx)
	}
	wg.Wait()
	return res / allWeights
}

func getWeightedVote(votes, weights []float64) float64 {
	var res, allWeights float64
	for idx := range votes {
		res += votes[idx] * weights[idx]
		allWeights += weights[idx]
	}
	return res / allWeights
}

func getWeightedVoteBce(votes, weights []float64) float64 {
	if len(votes) != len(weights) {
		panic(fmt.Errorf("votes and weights should be the same length, but were: %d and %d", len(votes), len(weights)))
	}
	var res, allWeights float64
	for idx := range votes {
		res += votes[idx] * weights[idx]
		allWeights += weights[idx]
	}
	return res / allWeights
}

func getWeightedVoteBlas64(votes, weights []float64) float64 {
	vec1 := blas64.Vector{
		N:    len(votes),
		Data: votes,
		Inc:  1,
	}
	vec2 := blas64.Vector{
		N:    len(weights),
		Data: weights,
		Inc:  1,
	}

	return blas64.Dot(vec1, vec2) / blas64.Asum(vec1)
}

func getWeightedVoteBlas64Vec(vec1, vec2 blas64.Vector) float64 {
	return blas64.Dot(vec1, vec2) / blas64.Asum(vec1)
}

func getWeightedVoteGonumMat(votes, weights []float64) float64 {
	vec1 := mat.NewVecDense(len(votes), votes)
	vec2 := mat.NewVecDense(len(weights), weights)
	return mat.Dot(vec1, vec2) / mat.Sum(vec2)
}

func getWeightedVoteGonumVecMat(vec1, vec2 mat.Vector) float64 {
	return mat.Dot(vec1, vec2) / mat.Sum(vec2)
}
